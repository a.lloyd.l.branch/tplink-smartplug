#!/usr/bin/env python
# 
# TP-Link Wi-Fi Smart Plug Protocol Client
# For use with TP-Link HS-100 or HS-110
#  
# by Lubomir Stroetmann
# Copyright 2016 softScheck GmbH 
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
#
import socket
import argparse
import os.path
import json

version = 0.1

# Check if IP is valid
def validIP(ip):
	try:
		socket.inet_pton(socket.AF_INET, ip)
	except socket.error:
		parser.error("Invalid IP Address.")
	return ip 

# Predefined Smart Plug Commands
# For a full list of commands, consult tplink_commands.txt
commands = {'info'     : '{"system":{"get_sysinfo":{}}}',
			'on'       : '{"system":{"set_relay_state":{"state":1}}}',
			'off'      : '{"system":{"set_relay_state":{"state":0}}}',
			'cloudinfo': '{"cnCloud":{"get_info":{}}}',
			'wlanscan' : '{"netif":{"get_scaninfo":{"refresh":0}}}',
			'time'     : '{"time":{"get_time":{}}}',
			'schedule' : '{"schedule":{"get_rules":{}}}',
			'countdown': '{"count_down":{"get_rules":{}}}',
			'antitheft': '{"anti_theft":{"get_rules":{}}}',
			'reboot'   : '{"system":{"reboot":{"delay":1}}}',
			'reset'    : '{"system":{"reset":{"delay":1}}}'
}

# Encryption and Decryption of TP-Link Smart Home Protocol
# XOR Autokey Cipher with starting key = 171
def encrypt(string):
	key = 171
	result = "\0\0\0\0"
	for i in string: 
		a = key ^ ord(i)
		key = a
		result += chr(a)
	return result

def decrypt(string):
	key = 171 
	result = ""
	for i in string: 
		a = key ^ ord(i)
		key = ord(i) 
		result += chr(a)
	return result.encode('utf-8')

# Parse commandline arguments
parser = argparse.ArgumentParser(description="TP-Link Wi-Fi Smart Plug Client v" + str(version))
parser.add_argument("-t", "--target", metavar="<ip>", required=True, help="Target IP Address", type=validIP)
parser.add_argument("-r","--raw", metavar="<yes/no>", help="Output RAW Json", choices=['yes','no'])
parser.add_argument("-f","--file",metavar="</path/to/file/name>", help="File name to create")
parser.add_argument("-ts","--timestamp", metavar="<yes/no>", help="Include a timestamp in file name. E.g. tpl_output_meter_20180619003845.json", choices=['yes','no'])
#parser.add_argument("-out","--output",metavar="Output file type", help="File type to output to",choices=['txt','json'])
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument("-c", "--command", metavar="<command>", help="Preset command to send. Choices are: "+", ".join(commands), choices=commands) 
group.add_argument("-j", "--json", metavar="<JSON string>", help="Full JSON string of command to send")
args = parser.parse_args()

# Set target IP, port and command to send
ip = args.target
port = 9999
if args.command is None:
	cmd = args.json
else:
	cmd = commands[args.command]

# Send command and receive reply 
try:
	sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock_tcp.connect((ip, port))
	sock_tcp.send(encrypt(cmd))
	data = sock_tcp.recv(2048)
	sock_tcp.close()
	
	data = decrypt(data[4:])
	# data_str = data.decode('utf-8')

	if args.raw == 'no':
		data = "Sent:     " + cmd + "\nReceived: " + data

	if args.file is None:
		# print(data)
		data_decoded = data.decode('utf-8')
		# print(data_decoded)
		# print(dir(data))
		# print(dir(data_str))
		# print(data_str)
		# print("LB - feature: " + jdata['system']['get_sysinfo'][0]['feature'])
	else:
		filename = args.file
		if os.path.isfile(filename) is False:
			with open(filename,'a') as myFile:
				myFile.write(calendar.timegm(time.gmtime()) + ":")
				myFile.close()
		if os.path.isfile(filename) is True:
			if str(filename).endswith('.json'):
				with open(filename) as f:
					filedata = json.load(f)
					filedata.update(data)
					print("")

		with open(filename,'a') as myFile:
			myFile.write(data)
			myFile.close()

except socket.error:
	quit("Cound not connect to host " + ip + ":" + str(port))
